module.exports = {
    "extends": "airbnb-base",
    "settings": {
        "import/resolver": {
            "node": {
                "extensions": [".js"]
            }
        },
    },
    "parser": "babel-eslint",
    "plugins": [
        "jest"
    ],
    "env": {
        "node": true,
        "jest/globals": true
    },
    "globals": {
        "i18n": false
    }
};
