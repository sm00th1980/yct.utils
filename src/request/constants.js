export const TIMEOUT = 1500; // ms
export const NETWORK_FAILED = 'Network Error';
export const TIMEOUT_REACHED = `timeout of ${TIMEOUT}ms exceeded`;
