/* eslint import/prefer-default-export: off */

import { NETWORK_FAILED, TIMEOUT_REACHED } from '../constants';

export const postDef = (
  post,
  errorTitle,
  errorMessage,
  url,
  params,
) => post(url, params)
  .then(response => response.data)
  .catch((error) => {
    if (error.message === NETWORK_FAILED || error.message === TIMEOUT_REACHED) {
      const err = errorMessage;
      err.name = errorTitle;

      throw err;
    } else {
      throw error;
    }
  });
