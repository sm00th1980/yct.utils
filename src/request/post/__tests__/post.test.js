import { postDef as post } from '..';

describe('post()', () => {
  it('make post request with success', async () => {
    expect.assertions(3);
    const response = 'response';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockPost = jest.fn(() => Promise.resolve({ data: response }));
    const errorTitle = 'errorTitle';
    const errorMessage = 'errorMessage';
    const url = 'url';
    const params = { param1: 1, param2: 2 };

    await post(mockPost, errorTitle, errorMessage, url, params)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(response);
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockPost).toHaveBeenCalledWith(url, params);
  });

  it('make post request with failure', async () => {
    expect.assertions(3);
    const error = new Error();

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockPost = jest.fn(() => Promise.reject(error));
    const errorTitle = 'errorTitle';
    const errorMessage = 'errorMessage';
    const url = 'url';
    const params = { param1: 1, param2: 2 };

    await post(mockPost, errorTitle, errorMessage, url, params)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);

    expect(mockPost).toHaveBeenCalledWith(url, params);
  });
});
