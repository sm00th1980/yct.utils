import { partial } from 'ramda';
import axios from 'axios';
import { TIMEOUT } from './constants';

import { getDef } from './get';
import { postDef } from './post';

let ERROR_TITLE;
let ERROR_MESSAGE;

export const setup = ({
  baseURL, errorTitle, errorMessage, timeout,
}) => {
  axios.defaults.baseURL = baseURL;
  axios.defaults.timeout = timeout || TIMEOUT; // timeout for abort

  ERROR_TITLE = errorTitle;
  ERROR_MESSAGE = errorMessage;
};

export const get = partial(getDef, [
  axios.get,
  ERROR_TITLE,
  ERROR_MESSAGE,
]);

export const post = partial(postDef, [
  axios.post,
  ERROR_TITLE,
  ERROR_MESSAGE,
]);
