/* eslint import/prefer-default-export: off */

import withQuery from 'with-query';
import { NETWORK_FAILED, TIMEOUT_REACHED } from '../constants';

export const getDef = (
  get,
  errorTitle,
  errorMessage,
  url,
  params,
) => get(
  withQuery(url, params),
)
  .then(response => response.data)
  .catch((error) => {
    if (error.message === NETWORK_FAILED || error.message === TIMEOUT_REACHED) {
      const err = new Error(errorMessage);
      err.name = errorTitle;

      throw err;
    } else {
      throw error;
    }
  });
