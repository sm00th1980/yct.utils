import { getDef as get } from '..';

describe('get()', () => {
  it('make get request with success without params', async () => {
    expect.assertions(3);
    const response = 'response';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn(() => Promise.resolve({ data: response }));
    const errorTitle = 'errorTitle';
    const errorMessage = 'errorMessage';
    const url = '/url';
    const mockParams = {};

    await get(mockGet, errorTitle, errorMessage, url, mockParams)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(response);
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith(url);
  });

  it('make get request with success with params', async () => {
    expect.assertions(3);
    const response = 'response';

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn(() => Promise.resolve({ data: response }));
    const errorTitle = 'errorTitle';
    const errorMessage = 'errorMessage';
    const url = '/url';
    const mockParams = {
      param1: 'param1',
    };

    await get(mockGet, errorTitle, errorMessage, url, mockParams)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).toHaveBeenCalledWith(response);
    expect(onFailure).not.toHaveBeenCalled();

    expect(mockGet).toHaveBeenCalledWith(`${url}?param1=param1`);
  });

  it('make get request with failure', async () => {
    expect.assertions(3);
    const error = new Error();

    const onSuccess = jest.fn();
    const onFailure = jest.fn();

    const mockGet = jest.fn(() => Promise.reject(error));
    const errorTitle = 'errorTitle';
    const errorMessage = 'errorMessage';
    const url = '/url';
    const mockParams = {};

    await get(mockGet, errorTitle, errorMessage, url, mockParams)
      .then(onSuccess)
      .catch(onFailure);

    expect(onSuccess).not.toHaveBeenCalled();
    expect(onFailure).toHaveBeenCalledWith(error);

    expect(mockGet).toHaveBeenCalledWith(url);
  });
});
