import lodashDebounce from 'lodash.debounce';
import lodashThrottle from 'lodash.throttle';

import {
  isNil, toPairs, fromPairs, reject, propEq, find, isEmpty,
  indexOf, values, groupBy, times, curry, prepend, take,
  drop,
} from 'ramda';

export const noop = () => {};

export const debounce = lodashDebounce;
export const throttle = lodashThrottle;

export const defer = (fn) => {
  setTimeout(fn);
};

export const onlyDigits = (value) => {
  if (value) {
    return value
      .toString()
      .split('')
      .filter(letter => ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'].includes(letter))
      .join('');
  }

  return '';
};

export const sample = items => items[Math.floor(Math.random() * items.length)];

export const withoutNilValues = (obj) => {
  const filterByNotNil = ([, value]) => !isNil(value);
  return fromPairs(toPairs(obj).filter(filterByNotNil));
};

export const compact = items => reject(item => isNil(item), items);

export const findById = (id, items) => find(propEq('id', id), items);

export const findIndex = (item, items) => {
  if (isNil(item) || isNil(items) || isEmpty(items)) {
    return -1;
  }

  return indexOf(item, items);
};

export const splitByParts = (count, list) => {
  const groupFn = ({ index }) => index % count;
  const result = values(groupBy(groupFn, list.map((element, index) => ({ element, index }))))
    .map(elements => elements.map(({ element }) => element));

  return [...result, ...times(() => [], count - result.length)];
};

export const sampleByIndex = (index, length, items) => {
  if (length === items.length) {
    return items[index];
  }

  if (items.length > length) {
    const percents = 1 / (length + 1) * index + 1 / (length + 1);
    const newIndex = Math.floor(items.length * percents);
    return items[newIndex];
  }

  const percents = (index * 100 / length) / 100;
  const newIndex = Math.floor(items.length * percents);

  return items[newIndex];
};

export const chunk = curry(function group(n, list) {
  return isEmpty(list) ? [] : prepend(take(n, list), group(n, drop(n, list)));
});
