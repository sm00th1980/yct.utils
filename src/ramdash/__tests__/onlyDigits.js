import { onlyDigits } from '..';

describe('onlyDigits', () => {
  it('return only digits', () => {
    expect(onlyDigits('123')).toEqual('123');
    expect(onlyDigits(123)).toEqual('123');
  });

  it('return only digits', () => {
    expect(onlyDigits('1d23')).toEqual('123');
    expect(onlyDigits('0x1d23')).toEqual('0123');
    expect(onlyDigits('0x123')).toEqual('0123');
  });

  it('return only digits', () => {
    expect(onlyDigits(null)).toEqual('');
    expect(onlyDigits(undefined)).toEqual('');
  });
});
