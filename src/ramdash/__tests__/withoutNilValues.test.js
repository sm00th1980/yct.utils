import { withoutNilValues } from '..';

describe('withoutNilValues', () => {
  it('reject undefined/null values', () => {
    expect(withoutNilValues({ test: 1 })).toEqual({ test: 1 });
    expect(withoutNilValues({ test: 1, style: undefined })).toEqual({ test: 1 });
    expect(withoutNilValues({ className: 'form-check', style: undefined })).toEqual({ className: 'form-check' });
    expect(withoutNilValues({ style: undefined })).toEqual({});
    expect(withoutNilValues({ style: null })).toEqual({});
  });
});
