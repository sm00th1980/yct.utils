import { chunk } from '..';

describe('chunk', () => {
  it('split array by size', () => {
    expect(chunk(2, [1, 2, 3])).toEqual([
      [1, 2],
      [3],
    ]);

    expect(chunk(3, [1, 2, 3, 4])).toEqual([
      [1, 2, 3],
      [4],
    ]);
  });
});
