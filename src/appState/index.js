import { partial } from 'ramda';
import { Observable } from 'rxjs';
import {
  distinctUntilChanged, share, filter, take,
} from 'rxjs/operators';

let APP_STATE;

export const appStateChangeDef$ = (appState) => {
  const source = Observable.create((observer) => {
    const handleChange = (nextAppState) => {
      if (nextAppState === 'active') {
        observer.next(true);
      } else {
        observer.next(false);
      }
    };

    appState.addEventListener('change', handleChange);

    return () => {
      appState.removeEventListener('change', handleChange);
    };
  });

  return source
    .pipe(distinctUntilChanged(), share());
};


export const waitUntilAppStateActiveDef$ = stateChange$ => stateChange$
  .pipe(
    filter(isActive => isActive),
    take(1),
  );


// export
export const setup = (AppState) => {
  APP_STATE = AppState;
};
export const appStateChange$ = partial(appStateChangeDef$, [APP_STATE]);
export const waitUntilAppStateActive$ = partial(waitUntilAppStateActiveDef$, [appStateChange$()]);
