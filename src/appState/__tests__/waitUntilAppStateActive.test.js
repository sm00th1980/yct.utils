import { waitUntilAppStateActiveDef$ as waitUntilAppStateActive$ } from '..';
import { of } from 'rxjs';

describe('waitUntilAppStateActive$()', () => {
  it('waitUntilAppStateActive$', (done) => {
    expect.assertions(2);

    const onNext = jest.fn();
    const onError = jest.fn();

    const mockStateChange = of(false, true);

    waitUntilAppStateActive$(mockStateChange)
      .subscribe(
        onNext,
        onError,
        () => {
          expect(onError).not.toHaveBeenCalled();
          expect(onNext.mock.calls).toEqual([
            [true],
          ]);

          done();
        },
      );
  });
});
