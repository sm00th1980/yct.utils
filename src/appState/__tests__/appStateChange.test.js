import { appStateChangeDef$ as appStateChange$ } from '..';
import { take } from 'rxjs/operators';

describe('appStateChange$()', () => {
  it('appStateChange$', (done) => {
    expect.assertions(2);

    const onNext = jest.fn();
    const onError = jest.fn();

    const mockAddEventListeners = jest.fn((_, cb) => {
      cb('active');
      cb('inactive');
      cb('background');
    });

    const mockAppState = {
      addEventListener: mockAddEventListeners,
      removeEventListener: () => {},
    };

    appStateChange$(mockAppState)
      .pipe(take(2))
      .subscribe(
        onNext,
        onError,
        () => {
          expect(onError).not.toHaveBeenCalled();
          expect(onNext.mock.calls).toEqual([
            [true],
            [false],
          ]);
          done();
        },
      );
  });
});
