/* eslint import/prefer-default-export: off */

export const minWait = (promise, ms) => Promise.all([
  promise,
  new Promise(resolve => setTimeout(resolve, ms)),
])
  .then(values => values[0]);
