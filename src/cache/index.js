import { is, isNil, equals } from 'ramda';
import { DEFAULT_EXPIRATION, INFINITE_EXPIRATION } from './constants';

let instance = null;

class Singleton {
  constructor() {
    if (!instance) {
      instance = this;
    }

    return instance;
  }

  set(key, value, expiration) {
    if (expiration === 0) {
      // don't need to cache it
      delete this[key]; // clean it for help GC
    }

    if (expiration > 0) {
      this[key] = { value, expiration, setDate: Date.now() };
    }

    if (expiration === INFINITE_EXPIRATION) {
      // cache it forever
      const currentYear = new Date().getFullYear();
      this[key] = {
        value,
        expiration: 0,
        setDate: new Date(currentYear + 50, 1, 1),
      };
    }
  }

  get(key) {
    const result = this[key];
    if (result) {
      const now = Date.now();

      if (now > result.setDate + result.expiration) {
        delete this[key]; // clean it for help GC
      } else {
        return result.value;
      }
    }

    return null;
  }

  drop(key) {
    const result = this[key];
    if (result) {
      delete this[key];
    }
  }
}

class Cache {
  static get(key) {
    const cache = new Singleton();
    return cache.get(key);
  }

  static set(key, value, expiration = DEFAULT_EXPIRATION) {
    const cache = new Singleton();
    cache.set(key, value, expiration);
  }

  static key(...args) {
    return args.join('_');
  }

  static drop(key) {
    const cache = new Singleton();
    return cache.drop(key);
  }

  static expiration(expiration, defaultValue) {
    const parsedExpiration = parseInt(expiration, 10);
    if (is(Number, parsedExpiration) && !equals(NaN, parsedExpiration)) {
      return parsedExpiration;
    }

    return defaultValue;
  }

  static exists(key) {
    const cache = new Singleton();
    return !isNil(cache.get(key));
  }
}

export default Cache;
