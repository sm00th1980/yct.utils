import CacheService from '..';
import { INFINITE_EXPIRATION } from '../constants';

describe('cache service', () => {
  describe('cache service', () => {
    it('should get saved value', () => {
      const key = 'key';
      const value = 'value';

      CacheService.set(key, value);

      expect(CacheService.get(key)).toEqual(value);
    });
  });

  describe('cache service with timeout', () => {
    it('should get saved value', () => {
      const key = 'key';
      const value = 'value';

      CacheService.set(key, value, 100); // save value only 100ms

      expect(CacheService.get(key)).toEqual(value);
    });

    it('should get saved value before timeout reached', async () => {
      const key = 'key';
      const value = 'value';

      CacheService.set(key, value, 100); // save value only 100ms
      await new Promise((resolve) => {
        setTimeout(() => {
          expect(CacheService.get(key)).toEqual(value);
          resolve();
        }, 10);
      });
    });

    it('should not get saved value after timeout reached', async () => {
      const key = 'key';
      const value = 'value';

      CacheService.set(key, value, 100); // save value only 100ms
      await new Promise((resolve) => {
        setTimeout(() => {
          expect(CacheService.get(key)).toBeNull();
          resolve();
        }, 110);
      });
    });
  });

  describe('cache service with zero timeout', () => {
    it('should not get saved value', () => {
      const key = 'key';
      const value = 'value';

      CacheService.set(key, value, 0); // dont need to cache

      expect(CacheService.get(key)).toBeNull();
    });

    it('should not  get saved value before timeout reached', async () => {
      const key = 'key';
      const value = 'value';

      CacheService.set(key, value, 0); // dont need to cache
      await new Promise((resolve) => {
        setTimeout(() => {
          expect(CacheService.get(key)).toBeNull();
          resolve();
        }, 110);
      });
    });
  });

  describe('cache service with infinite timeout', () => {
    it('should get saved value', () => {
      const key = 'key';
      const value = 'value';

      CacheService.set(key, value, INFINITE_EXPIRATION); // save value forever

      expect(CacheService.get(key)).toEqual(value);
    });

    it('should get saved value before infinite timeout reached', async () => {
      const key = 'key';
      const value = 'value';

      CacheService.set(key, value, INFINITE_EXPIRATION); // save value forever
      await new Promise((resolve) => {
        setTimeout(() => {
          expect(CacheService.get(key)).toEqual(value);
          resolve();
        }, 10);
      });
    });

    // xit('should not get saved value before infinite timeout reached', function () {
    //   const currentYear = new Date().getFullYear();
    //   const nextYear = new Date(currentYear + 1, 1, 1);
    //   jasmine.clock().mockDate(nextYear);

    //   expect(CacheService.get(this.key)).toEqual(this.value);
    // });
  });
});

describe('cache service', () => {
  it('should generate key of one argument', () => {
    expect(CacheService.key('1')).toEqual('1');
  });

  it('should generate key of two arguments', () => {
    expect(CacheService.key('1', '2')).toEqual('1_2');
    expect(CacheService.key('like', 123)).toEqual('like_123');
  });

  it('should generate key of three arguments', () => {
    expect(CacheService.key('1', '2', '3')).toEqual('1_2_3');
  });

  it('should generate key of four arguments', () => {
    expect(CacheService.key('1', '2', '3', '4')).toEqual('1_2_3_4');
  });

  it('should generate key of five arguments', () => {
    expect(CacheService.key('1', '2', '3', '4', '5')).toEqual('1_2_3_4_5');
  });
});

describe('cache service expiration', () => {
  it('should replace invalid expiration to default value', () => {
    const defaultValue = 100;

    expect(CacheService.expiration(undefined, defaultValue)).toEqual(
      defaultValue,
    );
    expect(CacheService.expiration(NaN, defaultValue)).toEqual(defaultValue);
    expect(CacheService.expiration(Infinity, defaultValue)).toEqual(
      defaultValue,
    );
    expect(CacheService.expiration(-Infinity, defaultValue)).toEqual(
      defaultValue,
    );
    expect(CacheService.expiration('vdfjknvnkdf', defaultValue)).toEqual(
      defaultValue,
    );
    expect(CacheService.expiration('vd12fjkn434vnkdf', defaultValue)).toEqual(
      defaultValue,
    );
    expect(CacheService.expiration(null, defaultValue)).toEqual(defaultValue);
  });

  it('should return valid expiration', () => {
    const defaultValue = 100;

    expect(CacheService.expiration(0, defaultValue)).toEqual(0);
    expect(CacheService.expiration(100, defaultValue)).toEqual(100);
    expect(CacheService.expiration(150, defaultValue)).toEqual(150);
    expect(CacheService.expiration(175.67, defaultValue)).toEqual(175);
    expect(CacheService.expiration('200', defaultValue)).toEqual(200);
    expect(CacheService.expiration('345.89123', defaultValue)).toEqual(345);
  });
});

describe('cache service drop', () => {
  it('should drop value', () => {
    const key = 'key';
    const value = 'value';

    CacheService.set(key, value, INFINITE_EXPIRATION);

    expect(CacheService.get(key)).toEqual(value);

    CacheService.drop(key);

    expect(CacheService.get(key)).toBeNull();
  });
});

describe('cache service - .exists(key)', () => {
  it('should check exists of value', () => {
    const key = 'key';
    const value = 'value';

    expect(CacheService.exists(key)).toBe(false);

    CacheService.set(key, value, INFINITE_EXPIRATION);
    expect(CacheService.exists(key)).toBe(true);

    CacheService.drop(key);
    expect(CacheService.exists(key)).toBe(false);
  });
});
