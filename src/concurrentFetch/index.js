import { is } from 'ramda';

import BluebirdPromise from 'bluebird';
import CacheService from '../cache';
import { INFINITE_EXPIRATION } from '../cache/constants';

// setup bluebird
global.Promise = BluebirdPromise;
global.Promise.config({
  warnings: true,
  longStackTraces: true,
  cancellation: true,
  monitoring: true,
});

class ConcurrentFetch {
  static oneOnly(key, fetcher, expiration = INFINITE_EXPIRATION) {
    const futureKey = CacheService.key(key);
    const cachedFuture = CacheService.get(futureKey);

    if (ConcurrentFetch.isPending(cachedFuture)) {
      return cachedFuture;
    }

    // start fetching
    const promise = fetcher().finally(() => {
      CacheService.drop(futureKey);
    });

    CacheService.set(futureKey, promise, expiration);

    return promise;
  }

  static key(...args) {
    return args.join('_');
  }

  static isPending(future) {
    // bluebird promise
    if (future && is(Function, future.isPending)) {
      return future.isPending();
    }

    return false;
  }
}

export default ConcurrentFetch;
