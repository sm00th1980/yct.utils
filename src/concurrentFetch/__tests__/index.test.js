import ConcurrentFetchService from '..';
import CacheService from '../../cache';

describe('ConcurrentFetchService - .oneOnly()', () => {
  let promise;
  let key;
  let fetcher;

  beforeEach(() => {
    key = 'this is the key';

    fetcher = () => new Promise((resolve) => {
      setTimeout(() => {
        resolve(123);
      }, 100);
    });

    expect(CacheService.exists(key)).toBe(false);
    promise = ConcurrentFetchService.oneOnly(key, fetcher);
    expect(promise.isPending()).toBe(true);
  });

  afterEach(() => {
    CacheService.drop(key);
  });

  it('save promise in cache', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        expect(CacheService.exists(key)).toBe(true);
        expect(promise.isPending()).toBe(true);

        resolve();
      }, 10);
    });
  });

  it('return same promise next call', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        const samePromise = ConcurrentFetchService.oneOnly(key, fetcher);
        expect(samePromise).toBe(promise); // should be the same

        resolve();
      }, 20);
    });
  });

  it('drop promise from cache when it fetched', async () => {
    await new Promise((resolve) => {
      setTimeout(() => {
        expect(CacheService.exists(key)).toBe(false);

        resolve();
      }, 110);
    });
  });
});

describe('ConcurrentFetchService - .key()', () => {
  it('generate key of one argument', () => {
    expect(ConcurrentFetchService.key('1')).toEqual('1');
  });

  it('generate key of two arguments', () => {
    expect(ConcurrentFetchService.key('1', '2')).toEqual('1_2');
    expect(ConcurrentFetchService.key('like', 123)).toEqual('like_123');
  });

  it('generate key of three arguments', () => {
    expect(ConcurrentFetchService.key('1', '2', '3')).toEqual('1_2_3');
  });

  it('generate key of four arguments', () => {
    expect(ConcurrentFetchService.key('1', '2', '3', '4')).toEqual('1_2_3_4');
  });

  it('generate key of five arguments', () => {
    expect(ConcurrentFetchService.key('1', '2', '3', '4', '5')).toEqual(
      '1_2_3_4_5',
    );
  });
});
