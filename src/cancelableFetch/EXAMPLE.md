```
import { buildService } from 'cancelableFetch';

class VenuesLiveSearch extends Component {
  state = {
    value: '',
  }

  componentDidMount() {
    const { fetchVenues } = this.props;
    this.venuesService = buildService(fetchVenues);
    this.subscription = this.venuesService.subscribe(noop);
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
    this.venuesService = undefined;
  }

  handleChangeValue = (newValue) => {
    this.setState({
      value: newValue,
    }, () => {
      this.venuesService.next(newValue);
    });
  }
```
