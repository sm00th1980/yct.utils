import { partial } from 'ramda';
import { Subject, Observable } from 'rxjs';
import { switchMap, debounceTime } from 'rxjs/operators';

export const startPromise$ = (cancelablePromiseFn) => {
  const source = Observable.create((observer) => {
    const promise = cancelablePromiseFn()
      .then((response) => {
        observer.next(response);
        observer.complete();
      })
      .catch((error) => {
        observer.error(error);
      });

    return () => {
      promise.cancel();
    };
  });

  return source;
};

export const buildService = (fetchFn) => {
  const service = new Subject();
  return service.pipe(
    debounceTime(300),
    switchMap(
      value => startPromise$(
        partial(fetchFn, [value]),
      ),
    ),
  );
};
